@extends('main')

@section('title', '| Contact')

@section('content')

<div> Contact me, or not </div>

<div class="text-lg"> Test for classes</div>

<div class="flex flex-auto w-full text-center p-4">
    <form>
        <label>Email:</label>
        <input class="border-2 px-4" id="email">
        <label>Subject:</label>
        <input class="border-2 px-4" id="subject">
        <div class="p-4 justify-end">
        <textarea class="border-2 p-4" id="message">Tell me a joke... not a funny one</textarea>
        </div>
        <input type="submit" value="Send Message" class="bg-green-600 hover:bg-green-900 p-4" text="Button">
    </form>
</div>

@endsection