

@extends('main')

@section('title', '| Homepage')

@section('content')

@foreach($posts as $post)
    <div class="flex flex-row p-4 m-4 border-2 border-black rounded-t-lg hover:border-dashed">
        <div class="w-1/4 p-4 text-black">{{$post->title}} </div>
        <div class="w-1/2 p-4 text-center text-black">{{ substr($post->body, 0, 200 )}}{{ strlen($post->body) > 200 ? "..." : "" }} </div>
        <div class="w-1/4 p-4 text-right">
            <a href="{{ url('blog/'.$post->slug) }}" class="m-4 text-center text-black align-middle bg-green-200 border-2 border-black rounded hover:bg-green-400 max-h-12">Read More</a>
        </div>
    </div>
@endforeach

@endsection