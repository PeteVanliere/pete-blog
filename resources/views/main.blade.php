<!doctype html>
<html>
    <head>
        @include('partials._head')
    </head>
    <body class="bg-gradient-to-r from-slate-100 to-slate-300">
        @include('partials._messages')
        @include('partials._nav')
        @yield('content')
        @include('partials._footer')
    </body>
</html>