@if (Session::has('success'))

    <div class="text-center border bg-amber-400">
        <div class="text-xl font-bold text-red-700"> IT WORKED!</div>
        <div> {{ Session::get('success') }}</div>
    </div>
@endif

@if (count($errors) > 0)

    <div class="text-center border bg-amber-400">
        <div class="text-xl font-bold text-red-700"> IT HAS ERRORS!</div>
        @foreach ($errors->all() as $error)
            <div>{{ $error }}</div>
        @endforeach
    </div>
@endif