<div class="flex flex-col w-full p-8 text-center bg-gradient-to-r from-transparent via-orange-200">
    <div class="text-4xl">WARM FIRE HERE</div>
    <div class="text-base">not an angelfire, but a demonfire...</div>
</div>

<div class="h-5 border-b-2 rounded-b-full border-slate-200 bg-gradient-to-r from-transparent via-orange-200 "></div>

<div class="flex justify-center p-4 px-4 text-lg text-blue-500">
  <a class="px-7 hover:bg-yellow-100 rounded-lg {{ (request()->is('/')) ? 'rounded-lg bg-slate-300 text-blue-700' : ''}}" href="/">Home</a>
  <a class="px-7 hover:bg-yellow-100 rounded-lg {{ (request()->is('about')) ? 'rounded-lg bg-slate-300 text-blue-700' : ''}}" href="/about">About</a>
  <a class="px-7 hover:bg-yellow-100 rounded-lg {{ (request()->is('contact')) ? 'rounded-lg bg-slate-300 text-blue-700' : ''}}" href="/contact">Contact</a>
  <a class="px-7 hover:bg-yellow-100 rounded-lg {{ (request()->is('posts')) ? 'rounded-lg bg-slate-300 text-blue-700' : ''}}" href="/posts">All Posts</a>
</div>