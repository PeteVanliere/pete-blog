@extends('main')

@section('title', '| View Post')

@section('content')
<div class="grid grid-cols-2 p-2 m-4 border-2 ">
    <div class="pr-10 text-xl text-right">
        <div>Title:</div>
        <div>Slug:</div>
        <div>Body:</div>
    </div>

    <div>
        <div>{{ $post->title }}</div>
        <div class="text-blue-400"><a href="{{ route('blog.single', $post->slug) }}">{{ route('blog.single', $post->slug) }}</div>
        <div>{{ $post->body }}</div>
    </div>

    <div class="pr-10 text-right">
        <div>Created at: {{ date( 'M j, Y h:i a', strtotime($post->created_at)) }}</div>
        <div>Last Updated: {{ date( 'M j, Y h:i a', strtotime($post->updated_at)) }}</div>
    </div>

    <div>
        <a class="px-4 m-4 text-white bg-green-400 rounded hover:bg-green-700" href="/posts/{{$post->id}}/edit">Edit</a>

        <form action="{{ route('posts.destroy', $post->id) }}" type="submit" method="post" accept-charset="UTF-8">
            @method('delete')
            @csrf
            <input type="submit" value="Delete Post" class="px-4 m-4 text-white bg-red-600 rounded hover:bg-red-900">
        </form>

    </div>
</div>

@endsection