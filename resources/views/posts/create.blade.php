@extends('main')

@section('title', '| Create New Post')

@section('content')

<div class="grid p-4 m-4 text-center cols-2 outline">

    <div class="text-xl">Create New Post!</div>

    <form method="POST" action="{{ route('posts.store') }}" accept-charset="UTF-8">
         @csrf


    <div class="flex w-full text-center">
      <!--  <div class="w-1/2 bg-orange-300">A</div>  -->
        <label class="w-1/4 text-right">Title</label>
        <input class="w-1/2 border-2" name="title" type="text" id="title">
    </div>

    <div class="flex w-full text-center">
      <!--  <div class="w-1/2 bg-orange-300">A</div>  -->
        <label class="w-1/4 text-right">Slug</label>
        <input class="w-1/2 border-2" name="slug" type="text" id="slug" minlength="5" maxlength="255">
    </div>

<!-- Get rid of this one for the moment
        <label for="body">DO I Need this lable?</label>
-->
        <div class="flex flex-row m-4 focus:outline-none focus:ring focus:ring-violet-300">
            <label class="w-24 text-lg" for="body"></label>
            <textarea class="p-4 text-lg" id="body" name="body" rows="10" cols="75">
            </textarea>
        </div>


    <!--  Leave this one here for the one that used to work after I work on the slug stuff

        <label for="body">Post Body</label>
        <input class="px-4 border-2" name="body" type="text" id="body"> -->


        <input type="submit" value="Create Post" class="p-4 bg-green-600 hover:bg-green-900">

    </form>
</div>


@endsection

