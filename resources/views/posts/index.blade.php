@extends('main')

@section('title', '| All Posts')

@section('content')

<div class='flex flex-row m-4 border-2 justify-evenly'>

    <div class="p-4">All Posts
    </div>

    <div class="p-4 text-right">
        <a class="p-1 m-2 text-white align-top bg-green-400 border-2 rounded-lg" href="{{ route('posts.create')}}">Create a post</a>
    </div>

</div>

<div class="flex flex-row w-full p-4 border-2 border-slate-200">
    <div class="w-1/4 text-3xl text-right">All Posts:</div>
       <!-- <div class="text-2xl bg-red-400 ">All Posts</div> -->
    <div class="w-2/4"></div>
    <div class="w-1/4">
        <a class="p-1 text-white bg-green-400 border-2 rounded-lg border-slate-400" href="{{ route('posts.create')}}">Write New Post</a>
    </div>
</div>

<div class="flex justify-center flex-auto">
    <table class="justify-center p-4 text-center table-fixed border-slate-400">

        <thead class="border bg-gradient-to-r from-slate-500">
            <tr class="">
                <th>ID</th>
                <th>Title</th>
                <th>Body</th>
                <th>Created at</th>
                <th></th>
            </tr>
        </thead>

        <tbody class="border border-red-300">
            @foreach($posts as $post)
                <tr class="border">
                    <td class="p-4">{{$post->id}}</td>
                    <td class="text-right">{{$post->title}}</td>
                    <td class="p-4 m-4">{{ substr($post->body, 0, 50 )}}{{ strlen($post->body) > 50 ? "..." : "" }}</td>
                    <td class="p-4 m-4">{{ date('M j, Y', strtotime($post->created_at))}}</td>
                    <td><a class="px-4 m-2 text-white bg-orange-200 border-2 rounded-lg hover:bg-orange-500 border-slate-400" href="{{ route('posts.show', $post->id)}}">View</a>
                        <a class="px-4 m-2 text-white border-2 rounded-lg border-slate-400 bg-emerald-300 hover:bg-emerald-500" href="{{ route('posts.edit', $post->id)}}">Edit</a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>


<!--This is the nav for pagination -->

<div class="flex justify-center">
{{ $posts->links() }}
</div>

@endsection