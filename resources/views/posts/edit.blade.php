@extends('main')

@section('title', '| Edit Blog Post')

@section('content')



<form method="POST" action="{{ route('posts.update', $post->id) }}" accept-charset="UTF-8">
@csrf
@method('PUT')

<div class="p-10">
    <div>
        <div class="flex flex-row m-4">
            <label class="w-24 text-lg" for="title">New Title:</label>
            <input class="px-4 border-2" value="{{$post->title}}" name="title" type="text" id="title">
        </div>

        <div class="flex flex-row m-4">
            <label class="w-24 text-lg" for="title">New Slug:</label>
            <input class="px-4 border-2" value="{{$post->slug}}" name="slug" type="text" id="slug">
        </div>

        <div class="flex flex-row m-4 focus:outline-none focus:ring focus:ring-violet-300">
            <label class="w-24 text-lg" for="body">New Body</label>
            <textarea class="p-4 text-lg" id="body" name="body" rows="10" cols="75">
            {{$post->body}}
            </textarea>
        </div>
    </div>

    <div class="text-center">
        <div>Created at: {{ date( 'M j, Y h:i a', strtotime($post->created_at)) }}</div>
        <div>Last Updated: {{ date( 'M j, Y h:i a', strtotime($post->updated_at)) }}</div>
    </div>

    <div><!-- Just a blank div to keep the grid working -->
    </div>

    <div class="p-4 m-4 text-center">
        <a class="p-4 text-white bg-red-600 rounded hover:bg-red-800 hover:border-4 hover:border-black" href="{{ route('posts.show', $post->id)}}">Cancel</a>
        <input type="submit" value="Save Changes" class="p-4 text-white bg-green-400 rounded hover:bg-green-900 hover:border-4 hover:border-black">
    </div>
</div>
</form>



@endsection