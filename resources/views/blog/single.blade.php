@extends('main')

@section('title', "| $post->title")

@section('content')

    <div class="p-10 text-4xl text-center">{{$post->title}}</div>
    <div class="p-4 border-2 border-slate-200">{{$post->body}}</div>

@endsection